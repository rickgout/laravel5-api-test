---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)
<!-- END_INFO -->

#general
<!-- START_e2940d0e38d955cf68a7ae5742166221 -->
## api/v2/user/{user}

> Example request:

```bash
curl -X GET "http://localhost/api/v2/user/{user}" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v2/user/{user}",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
[]
```

### HTTP Request
`GET api/v2/user/{user}`

`HEAD api/v2/user/{user}`


<!-- END_e2940d0e38d955cf68a7ae5742166221 -->

<!-- START_bc99b9776a8a9cbbc5bd9379c84d3407 -->
## api/v2/test

> Example request:

```bash
curl -X GET "http://localhost/api/v2/test" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v2/test",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "error": "Unauthenticated."
}
```

### HTTP Request
`GET api/v2/test`

`HEAD api/v2/test`


<!-- END_bc99b9776a8a9cbbc5bd9379c84d3407 -->

<!-- START_a71c151af132886057e0008e19768862 -->
## api/v2/user

> Example request:

```bash
curl -X POST "http://localhost/api/v2/user" \
-H "Accept: application/json" \
    -d "attribute"="dolore" \

```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v2/user",
    "method": "POST",
    "data": {
        "attribute": "dolore"
},
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v2/user`

#### Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    attribute | string |  required  | 

<!-- END_a71c151af132886057e0008e19768862 -->

