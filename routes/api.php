<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('user/{user}', 'Api\TestController@user')->name('api.test');

Route::get('/user', 'Api\TestController@testUserScope')->name('api.user');

Route::group(['middleware' => 'auth:api'], function() {

    Route::get('/test', 'Api\TestController@testScope');



});

