<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Transformers\UserTransformer;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use League\Fractal\Serializer\JsonApiSerializer;

class TestController extends Controller
{
    function user(User $user) {
        return response()->json($user);
    }

    function testScope(Request $request) {
        return 'test';
    }

    function testUserScope(Request $request) {
        $users =
            Cache::remember('users.index', 5, function() {
                return fractal(User::with('posts')->paginate(50), new UserTransformer())
                    ->withResourceName('users')
                    ->parseIncludes('posts');
            });

        return response()->json($users, 200);
    }
}
