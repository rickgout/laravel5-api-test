<?php
namespace App\Exceptions;

use Exception;

class CustomException extends \Exception
{
    protected $redirect, $message, $code, $status;

    public function render()
    {
        if (request()->expectsJson()) {
            return response()->json([
                'error' => [
                    'message'   => $this->message,
                    'code'      => $this->code,
                    'status'    => $this->status
                ]
            ], $this->code);
        }

        flash($this->message);

        return $this->redirect ? redirect($this->redirect) : redirect('errors/404');
    }
}