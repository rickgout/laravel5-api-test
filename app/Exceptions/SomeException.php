<?php

namespace App\Exceptions;

class SomeException extends CustomException
{
    protected $redirect, $message, $code, $status;

    public function __construct(
        $redirect = "",
        $message = "Some exception has happened",
        $code = 423,
        $status = "Resource is locked")
    {
        $this->redirect = $redirect;
        $this->message = $message;
        $this->code = $code;
        $this->status = $status;
    }

    public function render()
    {
        $this->handleException($this->redirect, $this->message, $this->code, $this->status);
    }
}