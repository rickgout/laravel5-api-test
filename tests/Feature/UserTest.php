<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $user = factory(User::class)->create(['email' => 'rick@freshapple.nl']);

        dd($user);

        $this->get(route('api.test', ['user' => 1]))
            ->assertJson([
                'email' => 'rick@freshapple.nl'
            ]);
    }
}
